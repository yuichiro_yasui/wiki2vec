import os
import urllib
import pandas as pd
from wikipedia2vec import Wikipedia2Vec

from fastapi import FastAPI, Request
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from markupsafe import Markup
    
class EntityLinking(object):
    def __init__(self, model_file):
        self.model_file = model_file
        self.linker = Wikipedia2Vec.load(model_file)

    def get(self, w, topk=5):
        ents = self.linker.get_entity(w) or self.linker.get_word(w)
        if not ents:
            return []
        else:
            return [ self.to_dict(e,s) for e,s in self.linker.most_similar(ents, topk) ]
        
    def to_dict(self, e, s):
        is_entity, is_word = hasattr(e, 'title'), hasattr(e, 'text')
        d = {}
        if is_entity or is_word:
            d['label'] = e.title if is_entity else e.text
            d['count'] = int(e.count)
            d['doc_count'] = int(e.doc_count)
            d['type'] = 'entity' if is_entity else 'word'
            d['score'] = float(s)
        return d

app = FastAPI(
    title='Entity linking API for Wikipedia entity',
    description="This API receives a japanese word and returns related Wikipedia entities (=pages) and/or terms.",
    version="0.1",
)
templates = Jinja2Templates(directory=".")

MODEL_FILE = os.environ.get('MODEL', None)
linker = EntityLinking(MODEL_FILE)

def make_wiki_pv_url(labels, baseurl='https://pageviews.toolforge.org/'):
    options = dict(
        project='ja.wikipedia.org',
        platform='all-access',
        agent='user',
        redirects=0,
        range='latest-90',
        pages='|'.join(labels[:10])
    )
    return baseurl + '?' + urllib.parse.urlencode(options)

def make_wiki_url(word, baseurl='https://ja.wikipedia.org/wiki/'):
    return baseurl + urllib.parse.quote(word)

@app.get('/health')
def health():
    return {'status': 'ok'}

@app.get('/json')
async def api_entity_linking(text: str, limit: int = 20):
    ents = linker.get(text, topk=limit)
    labels = [ e['label'] for e in ents if e['type'] == 'entity' ]
    url = make_wiki_pv_url(labels)
    return { 'text': text, 'limit': limit, 'labels': labels, 'url': url, 'entities': ents }

@app.get('/', response_class=HTMLResponse)
async def api_entity_linking(request: Request, text: str, limit: int = 20):
    ents = linker.get(text, topk=limit)
    labels = [ e['label'] for e in ents if e['type'] == 'entity' ]
    words = [ e['label'] for e in ents if e['type'] == 'word' ]
    url = make_wiki_pv_url(labels)
    ents = [
        dict(**e, **{
            'link': Markup('<a href="{}">{}</a>'.format(make_wiki_url(e['label']), e['label']))
            if e['type'] == 'entity' else e['label']
        })
        for e in ents
    ]
    nents, nwords = len(labels), len(words)
    print(nents, nwords)
    return templates.TemplateResponse(
        'template.html',
        { 'request': request, 'text': text, 'url': url, 'ents': ents,
          'nrecords': nents+nwords, 'nents': nents, 'nwords': nwords }
    )
