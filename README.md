# REST API for Wikipedia2vec #

This repository is for a REST server of Wikipedia2vec.
Wikipedia2vec is an implementation of the entity linking from a specified word to Wikipedia entities and words.

### Quick usage ###

This app requires Python3.

To set up a environment, install the following packages;
```bash
python3 -m pip install wikipedia2vec fastapi uvicorn pandas
```

To run a server, type the following command;
```bash
export MODEL=<path-to-model-file>
python3 -m uvicorn wiki2vec_fastapi:app --reload --port 8000 --proxy-headers --forwarded-allow-ips "*"
```

To receive related words and/or entities from the specified word `text=<word>` using `curl` as follows;
```bash
curl "http://ENDPOINT_URL:8000/" --get --data-urlencode "text=bitbucket"
curl "http://ENDPOINT_URL:8000/json" --get --data-urlencode "text=bitbucket"
```
`/` returns HTML format and `/json` returns JSON format.